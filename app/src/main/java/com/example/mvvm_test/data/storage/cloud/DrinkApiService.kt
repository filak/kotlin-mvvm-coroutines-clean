package com.example.mvvm_test.data.storage.cloud

import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface DrinkApiService {
    @GET("json/v1/1/random.php")
    fun getRandomDrinkAsync(): Deferred<DrinksResponse>
}