package com.example.mvvm_test.data.storage.model

import com.google.gson.annotations.SerializedName

data class DrinkDto(
    @SerializedName("strDrink") var name: String,
    @SerializedName("strDrinkThumb") var thumb: String
)