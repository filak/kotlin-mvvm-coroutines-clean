package com.example.mvvm_test.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.mvvm_test.R
import com.example.mvvm_test.data.repository.model.Drink
import com.example.mvvm_test.ui.viewModel.DrinkViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class DrinkActivity : AppCompatActivity() {

    private val mDrinkViewModel: DrinkViewModel by lazy {
        ViewModelProviders.of(this).get(DrinkViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        subscribeToDrinkLiveData()

        setupControlButtons()
    }

    private fun subscribeToDrinkLiveData() {
        mDrinkViewModel.drinkLiveData.observe(this, Observer { drink ->
            Picasso.get().load(drink.thumb).into(thumb)
            name.text = drink.name
        })
    }

    private fun setupControlButtons() {
        get_drink.setOnClickListener {
            mDrinkViewModel.getRandomDrink()
        }

        open_save_list.setOnClickListener {
            val intent = Intent(this, SavedDrinksActivity::class.java)
            startActivity(intent)
        }

        save_drink.setOnClickListener {
            mDrinkViewModel.saveDrink()
        }
    }
}

