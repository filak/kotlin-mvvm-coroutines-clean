package com.example.mvvm_test.ui.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.mvvm_test.data.repository.model.Drink
import com.example.mvvm_test.domain.usecase.GetSavedDrinksUseCase

class SavedDrinksViewModel (application: Application) : AndroidViewModel(application) {

    fun subscribeToDatabase(): List<Drink>? {
        return GetSavedDrinksUseCase().getSavedDrinksList()
    }
}