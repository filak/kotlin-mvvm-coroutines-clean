package com.example.mvvm_test.ui.activity

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvm_test.R
import com.example.mvvm_test.ui.adapter.DrinkAdapter
import com.example.mvvm_test.ui.viewModel.SavedDrinksViewModel
import kotlinx.android.synthetic.main.activity_saved_drinks.*
import kotlinx.coroutines.*

class SavedDrinksActivity : AppCompatActivity() {
    private var scope: CoroutineScope = CoroutineScope(Job())
    private val savedDrinksViewModel: SavedDrinksViewModel by lazy {
        ViewModelProviders.of(this).get(
            SavedDrinksViewModel::class.java
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saved_drinks)
        recycler_view.layoutManager = LinearLayoutManager(this)

        scope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
               savedDrinksViewModel.subscribeToDatabase()
            }?.let { recycler_view.adapter = DrinkAdapter(ArrayList(it)) }
        }

    }
}
