package com.example.mvvm_test.ui.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.mvvm_test.data.repository.model.Drink
import com.example.mvvm_test.data.storage.database.room.DrinkDatabase
import com.example.mvvm_test.data.storage.database.room.DrinkDao
import com.example.mvvm_test.domain.usecase.GetRandomDrinkUseCase
import kotlinx.coroutines.*

class DrinkViewModel(application: Application) : AndroidViewModel(application) {

    var drinkLiveData: MutableLiveData<Drink> = MutableLiveData()
    private var scope = CoroutineScope(Job())
    private var currentDrink: Drink? = null

    override fun onCleared() {
        super.onCleared()
        DrinkDatabase.destroyDatabase()
    }

    fun getRandomDrink() {
//        scope.launch(Dispatchers.Main) {
            GetRandomDrinkUseCase().getRandomDrink { response ->
                if (response.drink != null) {
                    currentDrink = response.drink
                    drinkLiveData.value = currentDrink
                    //TODO proccesing error
                }
            }
//        }
    }

    fun saveDrink() {
        if (currentDrink != null) {
            scope.launch {
                val drinkDao = getDrinkDao()
                if (drinkDao?.getById(currentDrink!!.name) == null)
                    drinkDao?.insertDrink(currentDrink!!)
            }
        }
    }

    private fun getDrinkDao(): DrinkDao? =
        DrinkDatabase.getReadyDatabase()?.drinkDao
}