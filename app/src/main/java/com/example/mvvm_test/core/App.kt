package com.example.mvvm_test.core

import android.app.Application
import com.example.mvvm_test.data.storage.database.room.DrinkDatabase

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        DrinkDatabase.createAppDatabase(this)
    }
}