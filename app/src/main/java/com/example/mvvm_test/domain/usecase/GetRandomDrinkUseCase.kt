package com.example.mvvm_test.domain.usecase

import com.example.mvvm_test.data.repository.DrinkRepository
import com.example.mvvm_test.data.storage.cloud.DrinksResponse
import com.example.mvvm_test.domain.model.Response
import com.example.mvvm_test.domain.usecase.mapper.DrinkMapper
import kotlinx.coroutines.*
import retrofit2.HttpException

class GetRandomDrinkUseCase {
    private val scope = CoroutineScope(Job())
    private var drinkResponse: DrinksResponse? = null

    fun getRandomDrink(callback: (Response) -> Unit) {
        scope.launch(Dispatchers.Main) {
            try {
                drinkResponse = DrinkRepository().getRandomDrinkAsync().await()
                callback(
                    Response(
                        DrinkMapper().drinkDtoToDrink(drinkResponse?.drinkDtoList)?.get(0)
                    )
                )
            } catch (e: HttpException) {
                // Catch http errors
                callback(Response(error = e.toString()))
            } catch (e: Throwable) {
                callback(Response(error = e.toString()))
            }
        }

    }
}