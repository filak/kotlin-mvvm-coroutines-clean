package com.example.mvvm_test.domain.usecase.mapper

import com.example.mvvm_test.data.repository.model.Drink
import com.example.mvvm_test.data.storage.model.DrinkDto

class DrinkMapper {
    fun drinkDtoToDrink(drinkDtoList: List<DrinkDto>?): List<Drink>? {
        return drinkDtoList?.map { Drink(it.name, it.thumb) }
    }
}