package com.example.mvvm_test.domain.usecase

import androidx.lifecycle.LiveData
import com.example.mvvm_test.data.repository.DrinkRepository
import com.example.mvvm_test.data.repository.model.Drink

class GetSavedDrinksUseCase {
    fun getSavedDrinksList(): List<Drink>? {
        return DrinkRepository().getSavedDrinksList()
    }
}